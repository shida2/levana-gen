use anyhow::*;
use once_cell::sync::OnceCell;
use rand::seq::SliceRandom;
use rayon::prelude::*;
use serde::{Deserialize, Deserializer, Serialize, Serializer};
use serde_with::*;
use std::fs::File;
use std::io::prelude::*;
use std::path::{Path, PathBuf};
use std::str::FromStr;
use std::sync::{Arc, Mutex};

#[derive(Clone)]
struct Element {
    name: String,
    layer_folder: PathBuf,
    blend_method: cairo::Operator,
}

struct FinalImage {
    filename: String,
    elements: Vec<Element>,
}

#[derive(Clone, Copy, Debug, PartialEq, Serialize, Deserialize)]
enum Randomize {
    Shuffle,    // Default
    FakeRandom, // When combination.needed is less than full combination of layers, this ensures every element(s if in matching) appears at least once
    AsIs,
}

#[derive(Debug, Clone, PartialEq)]
struct BlendMethod(cairo::Operator);
impl core::fmt::Display for BlendMethod {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Blend method serde error")
    }
}
impl FromStr for BlendMethod {
    type Err = anyhow::Error;

    fn from_str(input: &str) -> Result<BlendMethod> {
        match input {
            "add" => Ok(BlendMethod(cairo::Operator::Add)),
            "atop" => Ok(BlendMethod(cairo::Operator::Atop)),
            "clear" => Ok(BlendMethod(cairo::Operator::Clear)),
            "color_burn" => Ok(BlendMethod(cairo::Operator::ColorBurn)),
            "color_dodge" => Ok(BlendMethod(cairo::Operator::ColorDodge)),
            "darken" => Ok(BlendMethod(cairo::Operator::Darken)),
            "dest" => Ok(BlendMethod(cairo::Operator::Dest)),
            "dest_atop" => Ok(BlendMethod(cairo::Operator::DestAtop)),
            "dest_in" => Ok(BlendMethod(cairo::Operator::DestIn)),
            "dest_out" => Ok(BlendMethod(cairo::Operator::DestOut)),
            "dest_over" => Ok(BlendMethod(cairo::Operator::DestOver)),
            "difference" => Ok(BlendMethod(cairo::Operator::Difference)),
            "exclusion" => Ok(BlendMethod(cairo::Operator::Exclusion)),
            "hard_light" => Ok(BlendMethod(cairo::Operator::HardLight)),
            "hsl_color" => Ok(BlendMethod(cairo::Operator::HslColor)),
            "hsl_hue" => Ok(BlendMethod(cairo::Operator::HslHue)),
            "hsl_luminosity" => Ok(BlendMethod(cairo::Operator::HslLuminosity)),
            "hsl_saturation" => Ok(BlendMethod(cairo::Operator::HslSaturation)),
            "in" => Ok(BlendMethod(cairo::Operator::In)),
            "lighten" => Ok(BlendMethod(cairo::Operator::Lighten)),
            "multiply" => Ok(BlendMethod(cairo::Operator::Multiply)),
            "out" => Ok(BlendMethod(cairo::Operator::Out)),
            "over" => Ok(BlendMethod(cairo::Operator::Over)),
            "overlay" => Ok(BlendMethod(cairo::Operator::Overlay)),
            "saturate" => Ok(BlendMethod(cairo::Operator::Saturate)),
            "screen" => Ok(BlendMethod(cairo::Operator::Screen)),
            "soft_light" => Ok(BlendMethod(cairo::Operator::SoftLight)),
            "source" => Ok(BlendMethod(cairo::Operator::Source)),
            "xor" => Ok(BlendMethod(cairo::Operator::Xor)),
            _ => Err(anyhow!("Unsupported Cairo operator")),
        }
    }
}

#[serde_as]
#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct SingleLayer {
    name: String,
    folder: String,
    #[serde_as(as = "DisplayFromStr")]
    blend_method: BlendMethod,
    randomize: Randomize,
    occurences: Vec<SingleOccurence>,
}

#[derive(Debug, PartialEq)]
struct SingleOccurence {
    filename: String,
    count: usize,
}
impl Serialize for SingleOccurence {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        (&self.filename, &self.count).serialize(serializer)
    }
}
impl<'de> Deserialize<'de> for SingleOccurence {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let (f, c) = serde::Deserialize::deserialize(deserializer)?;
        Ok(SingleOccurence {
            filename: f,
            count: c,
        })
    }
}

#[serde_as]
#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
struct MatchLayer {
    name: String,
    folders: Vec<String>,
    #[serde_as(as = "Vec<DisplayFromStr>")]
    blend_methods: Vec<BlendMethod>,
    randomize: Randomize,
    occurences: Vec<MatchOccurence>,
}

#[derive(Clone, Debug, PartialEq)]
struct MatchOccurence {
    filenames: Vec<String>,
    count: usize,
}
impl Serialize for MatchOccurence {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: Serializer,
    {
        (&self.filenames, &self.count).serialize(serializer)
    }
}
impl<'de> Deserialize<'de> for MatchOccurence {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let (f, c) = serde::Deserialize::deserialize(deserializer)?;
        Ok(MatchOccurence {
            filenames: f,
            count: c,
        })
    }
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
enum Layer {
    Single(SingleLayer),
    Match(MatchLayer),
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Combination {
    name: String,
    first_index: usize,
    layers: Vec<Layer>,
    needed: usize,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Size {
    width: i32,
    height: i32,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct Config {
    target_size: Size, // No source resizing yet.
    combinations: Vec<Combination>,
}

#[derive(Debug, PartialEq, Serialize, Deserialize)]
struct IpfsAddResult {
    // #[serde(rename = "Bytes")]
    // bytes: i64,
    #[serde(rename = "Hash")]
    hash: String,
    #[serde(rename = "Name")]
    name: String,
    #[serde(rename = "Size")]
    size: String,
}

#[derive(Debug, Serialize, Deserialize)]
struct PrevEgg {
    #[serde(rename = "CID")]
    cid: String,
    #[serde(rename = "Rarity Category")]
    rarity: String,
    #[serde(rename = "Egg File")]
    body: String,
    #[serde(rename = "Cave File")]
    cave: String,
    #[serde(rename = "Background File")]
    background: String,
    #[serde(rename = "Constilation File")]
    constilation: String,
    #[serde(rename = "Star File")]
    star: String,
    #[serde(rename = "Essence")]
    essence: String,
    #[serde(rename = "ID")]
    id: String,
}

fn main() -> Result<()> {
    rayon::ThreadPoolBuilder::new()
        .num_threads(6)
        .build_global()?;
    let mut cache = FileCache::default();
    let brand_new = true;
    let config = if brand_new {
        // From config
        serde_json::from_reader(File::open("eggs.json")?)?
    } else {
        // Or from previous CSV
        let mut rdr = csv::Reader::from_reader(File::open("egg-images.csv")?);
        let _grid = rdr
            .deserialize()
            .map(|row| {
                let prev_egg: PrevEgg = row?;
                Ok((
                    prev_egg.id,
                    vec![
                        prev_egg.background,
                        prev_egg.cave,
                        prev_egg.rarity.to_lowercase(),
                        prev_egg.essence.clone().to_lowercase(),
                        prev_egg.body,
                        prev_egg.essence.to_lowercase(),
                    ],
                ))
            })
            .collect::<Result<Vec<(String, Vec<String>)>>>()?;
        let fake_layers = vec![
            MatchLayer {
                name: "Backgrounds".to_string(),
                folders: vec!["Backgrounds".to_string()],
                blend_methods: vec![BlendMethod(cairo::Operator::Over)],
                randomize: Randomize::AsIs,
                occurences: vec![],
            },
            MatchLayer {
                name: "Caves".to_string(),
                folders: vec!["Caves".to_string()],
                blend_methods: vec![BlendMethod(cairo::Operator::SoftLight)],
                randomize: Randomize::AsIs,
                occurences: vec![],
            },
            MatchLayer {
                name: "Nests".to_string(),
                folders: vec!["Nests".to_string()],
                blend_methods: vec![BlendMethod(cairo::Operator::Over)],
                randomize: Randomize::AsIs,
                occurences: vec![],
            },
            MatchLayer {
                name: "Energies".to_string(),
                folders: vec!["Energies".to_string()],
                blend_methods: vec![BlendMethod(cairo::Operator::Over)],
                randomize: Randomize::AsIs,
                occurences: vec![],
            },
            MatchLayer {
                name: "Bodies".to_string(),
                folders: vec!["Bodies".to_string()],
                blend_methods: vec![BlendMethod(cairo::Operator::Over)],
                randomize: Randomize::AsIs,
                occurences: vec![],
            },
            MatchLayer {
                name: "Essences".to_string(),
                folders: vec!["Essences".to_string()],
                blend_methods: vec![BlendMethod(cairo::Operator::Over)],
                randomize: Randomize::AsIs,
                occurences: vec![],
            },
        ];
        Config {
            target_size: Size {
                width: 1200,
                height: 1200,
            },
            combinations: vec![Combination {
                name: "All".to_string(),
                first_index: 1,
                needed: 0,
                layers: fake_layers.into_iter().map(|l| Layer::Match(l)).collect(),
            }],
        }
    };
    let source_folder = Path::new("/home/shida/Downloads/layers");
    let target_folder = Path::new("/home/shida/Downloads/build");

    clean_build_folder(target_folder)?;
    let comb = &config.combinations;
    let images = expand(comb, source_folder);
    // let images_of_elems = grid
    //     .iter()
    //     .map(|(id, row)| {
    //         let image_of_elems = row
    //             .iter()
    //             .enumerate()
    //             .map(|(i, filename)| {
    //                 let layer = &fake_layers[i];
    //                 let mut full_path = source_folder.to_path_buf();
    //                 full_path.push(&layer.folders[0]);
    //                 full_path.push(filename);
    //                 full_path.set_extension("png");
    //                 let image = cache.get(full_path)?;
    //                 Ok(Element {
    //                     name: filename.clone(),
    //                     layer_folder: layer.name.clone(),
    //                     blend_method: read_operator(&layer.blend_methods[0])?,
    //                     image_data: image.into(),
    //                 })
    //             })
    //             .collect::<Result<Vec<_>>>()?;
    //         Ok((id, image_of_elems))
    //     })
    //     .collect::<Result<Vec<_>>>()?;
    // let images_of_elems_s = vec![images_of_elems];
    let damn = |fi: &FinalImage| {
        let target_surface = gen_image(fi, &config.target_size, &mut cache)?;
        let mut full_path = target_folder.to_path_buf(); // miss layer folder name
        full_path.push(fi.filename.clone());
        // full_path.push(id);
        full_path.set_extension("png");
        println!("Write {:?}", full_path);
        {
            let mut w = File::create(full_path.clone())?;
            target_surface.write_to_png(&mut w)?;
        }

        let client = reqwest::blocking::Client::new();
        let form = reqwest::blocking::multipart::Form::new().file("file", full_path)?;
        let res = client
            .post("http://127.0.0.1:5001/api/v0/add")
            .multipart(form)
            .send()?;
        let mut r = if res.status() == reqwest::StatusCode::OK {
            vec![res.json::<IpfsAddResult>()?.hash, fi.filename.clone()]
        } else {
            println!("Failed uploading {}.png to IPFS", fi.filename.clone());
            println!("{}", res.text()?);
            vec![fi.filename.clone(), fi.filename.clone()]
        };

        let mut names = fi.elements.iter().map(|e| e.name.clone()).collect();
        r.append(&mut names);
        Ok(r)
    };
    let results = images.par_iter().map(damn).collect::<Result<Vec<_>>>();
    let mut wtr = csv::Writer::from_writer(std::io::stdout());
    results
        .iter()
        .map(|comb| {
            comb.iter()
                .map(|image| {
                    wtr.serialize(image)?;
                    Ok(())
                })
                .collect::<Result<Vec<_>>>()?;
            Ok(())
        })
        .collect::<Result<Vec<_>>>()?;
    Ok(())
}

fn clean_build_folder(folder: &Path) -> Result<()> {
    std::fs::remove_dir_all(folder).unwrap_or(());
    std::fs::create_dir_all(folder)?;
    Ok(())
}

fn load_src_images(fi: &FinalImage, mut fc: FileCache) -> Result<()> {
    fi.elements
        .iter()
        .map(|e| {
            let mut sub_path = PathBuf::new();
            sub_path.push(e.layer_folder.clone());
            sub_path.push(e.name.clone());
            sub_path.set_extension("png");
            fc.get(sub_path.as_path())?;
            Ok(())
        })
        .collect()
}

fn transpose<T>(v: Vec<Vec<T>>) -> Vec<Vec<T>> {
    if v.is_empty() {
        vec![]
    } else {
        let len = v[0].len();
        let mut iters: Vec<_> = v.into_iter().map(|n| n.into_iter()).collect();
        (0..len)
            .map(|_| {
                iters
                    .iter_mut()
                    .map(|n| n.next().unwrap())
                    .collect::<Vec<T>>()
            })
            .collect()
    }
}

fn single_to_match(wrapped_layer: &Layer) -> MatchLayer {
    match wrapped_layer {
        Layer::Match(ml) => (*ml).clone(),
        Layer::Single(sl) => MatchLayer {
            name: sl.name.clone(),
            folders: vec![sl.folder.clone()],
            blend_methods: vec![sl.blend_method.clone()],
            randomize: sl.randomize,
            occurences: sl
                .occurences
                .iter()
                .map(|so| MatchOccurence {
                    filenames: vec![so.filename.clone()],
                    count: so.count,
                })
                .collect(),
        },
    }
}

fn expand_layer(
    randomize: Randomize,
    loaded_occur: Vec<(Vec<Element>, usize)>,
) -> Vec<Vec<Element>> {
    match randomize {
        Randomize::AsIs => loaded_occur
            .into_iter()
            .flat_map(|(es, c)| std::iter::repeat(es).take(c).collect::<Vec<Vec<Element>>>())
            .collect(),
        Randomize::FakeRandom => {
            let mut uniq: Vec<Vec<Element>> =
                loaded_occur.iter().map(|(es, _)| es.clone()).collect();
            let leftover_occur: Vec<(Vec<Element>, usize)> = loaded_occur
                .into_iter()
                .filter_map(|(es, i)| if i - 1 > 0 { Some((es, i - 1)) } else { None })
                .collect();
            let mut rng = rand::thread_rng();
            let mut result: Vec<Vec<Element>> = leftover_occur
                .into_iter()
                .flat_map(|(es, c)| std::iter::repeat(es).take(c).collect::<Vec<Vec<Element>>>())
                .collect();
            result.shuffle(&mut rng);
            uniq.append(&mut result);
            uniq
        }
        Randomize::Shuffle => {
            let mut rng = rand::thread_rng();
            let mut result: Vec<Vec<Element>> = loaded_occur
                .into_iter()
                .flat_map(|(es, c)| std::iter::repeat(es).take(c).collect::<Vec<Vec<Element>>>())
                .collect();
            result.shuffle(&mut rng);
            result
        }
    }
}

fn gen_image(
    fi: &FinalImage,
    target_size: &Size,
    fc: &mut FileCache,
) -> Result<cairo::ImageSurface> {
    let target_surface =
        cairo::ImageSurface::create(cairo::Format::ARgb32, target_size.width, target_size.height)?;
    let target = cairo::Context::new(&target_surface)?;
    fi.elements
        .iter()
        .map(|elem| {
            let mut sub_path = PathBuf::new();
            sub_path.push(elem.layer_folder.clone());
            sub_path.push(elem.name.clone());
            sub_path.set_extension("png");
            let mut d: &[u8] = &*(fc.get(sub_path.as_path())?);
            let s = cairo::ImageSurface::create_from_png(&mut d)?;
            target.save()?;
            target.set_operator(elem.blend_method);
            target.set_source_surface(&s, 0.0, 0.0)?;
            target.paint_with_alpha(1.0)?;
            target.restore()?;
            Ok(())
        })
        .collect::<Result<()>>()?;
    Ok(target_surface)
}

fn expand(combinations: &Vec<Combination>, src_folder: &Path) -> Vec<FinalImage> {
    combinations
        .iter()
        .map(|combination| {
            let layers = &combination.layers;
            let layers = layers.iter().map(single_to_match);
            let layers_of_elems = layers
                .map(|layer| {
                    let l = layer
                        .occurences
                        .iter()
                        .map(|occur| {
                            (
                                occur
                                    .filenames
                                    .iter()
                                    .enumerate()
                                    .map(|(i, filename)| {
                                        let BlendMethod(o) = layer.blend_methods[i];
                                        Element {
                                            name: filename.clone(),
                                            layer_folder: [
                                                src_folder,
                                                Path::new(&layer.folders[i].clone()),
                                            ]
                                            .iter()
                                            .collect(),
                                            blend_method: o,
                                        }
                                    })
                                    .collect(),
                                occur.count,
                            )
                        })
                        .collect();
                    expand_layer(layer.randomize, l)
                })
                .collect();
            let mut images_of_elems: Vec<FinalImage> = transpose(layers_of_elems)
                .into_iter()
                .enumerate()
                .map(|(i, x)| FinalImage {
                    filename: (i + combination.first_index).to_string(),
                    elements: x.into_iter().flatten().collect(),
                })
                .collect();
            if combination.needed > 0 {
                images_of_elems = images_of_elems
                    .into_iter()
                    .take(combination.needed)
                    .collect()
            }
            images_of_elems
        })
        .flatten()
        .collect()
}

// FileCache, I do not want to handle mod yet.

use std::collections::HashMap;

#[derive(Debug, Default)]
struct FileCache {
    files: Arc<Mutex<HashMap<PathBuf, Arc<OnceCell<Arc<[u8]>>>>>>,
}

impl FileCache {
    fn get(&self, filename: &Path) -> Result<Arc<[u8]>> {
        let cell = {
            let mut files_lock = match self.files.lock() {
                Ok(l) => Ok(l),
                Err(_) => Err(anyhow!("Lock on FileCache is poisoned.")),
            }?;
            files_lock
                .entry(filename.to_path_buf())
                .or_insert_with(|| Arc::new(OnceCell::new()))
                .clone()
        };
        cell.get_or_try_init(|| {
            let mut f = File::open(filename).map_err(|err| {
                println!("Failed to open {}", filename.display());
                anyhow!(err)
            })?;
            let mut b = Vec::new();
            f.read_to_end(&mut b)?;
            Ok(b.into())
        })
        .map(|x| x.clone())
    }
}
